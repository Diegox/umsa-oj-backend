const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const cookieParser = require('cookie-parser');
const logger = require('morgan');
const dotenv = require('dotenv');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');

const usersRouter = require('./routes/users');
const problemsRouter = require('./routes/problems');
const testCasesRouter = require('./routes/testCases');
const submissionsRouter = require('./routes/submissions');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

dotenv.config({
  path: path.join(__dirname, './env')
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, { explorer: true }));
app.use('/api', usersRouter);
app.use('/api', problemsRouter);
app.use('/api', testCasesRouter);
app.use('/api', submissionsRouter);

// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

module.exports = app;
