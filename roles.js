const AccessControl = require('accesscontrol');
const ac = new AccessControl();

const roleTypes = {
  BASIC: 'basic',
  PROBLEM_EDITOR: 'problem_editor',
  PROBLEM_SETTER: 'problem_setter',
  ADMIN: 'admin'
}

ac.grant(roleTypes.BASIC)
    .readOwn('user')
    .updateOwn('user')
  .grant(roleTypes.PROBLEM_EDITOR)
    .extend(roleTypes.BASIC)
    .updateAny('problem')
  .grant(roleTypes.PROBLEM_SETTER)
    .extend(roleTypes.PROBLEM_EDITOR)
    .createAny('problem')
    .deleteOwn('problem')
  .grant(roleTypes.ADMIN)
    .extend(roleTypes.PROBLEM_SETTER)
    .readAny('user')
    .updateAny('user')
    .deleteAny('problem')
    .deleteAny('user');

module.exports = {
  roles: ac
}
