const { createLogger, format, transports } = require('winston');
const { splat, combine, timestamp, printf, colorize } = format;
 
const myFormat = printf(({ timestamp, level, message, meta }) => {
  return `${timestamp} - ${level} - ${message}`;
});

const logger = createLogger({
  format: combine(
    colorize(),
    timestamp(),
    splat(),
    myFormat
  ),
  transports: [
    new transports.Console({ })
  ]
});

module.exports = logger;
