const paginate = ({ page, pageSize }) => {
  const offset = page * pageSize;
  const limit = +pageSize;
  console.log('offset, limit:', offset, limit);
  return {
      offset,
      limit
  };
};

module.exports = {
  paginate
}
