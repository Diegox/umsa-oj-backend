const jwt = require('jsonwebtoken');
const { 
  JWT_SECRET,
  JWT_ISSUER,
  JWT_EXPIRES
 } = require('../settings');

const sign = (payload, options) => {
  const signingOptions = {
    expiresIn: JWT_EXPIRES,
    issuer: JWT_ISSUER,
    subject: options.subject
  };
  return jwt.sign(payload, JWT_SECRET, signingOptions);
}

const parseTokenFromAuthorizationHeader = req => {
  const authorizationHeader = req.headers['Authorization'];
  if (!authorizationHeader || !authorizationHeader.includes('Bearer ')) {
    return null;
  }
  return req.headers['Authorization'].split(' ')[1];
}

const verify = token => {
  const verifyOptions = {
    expiresIn: JWT_EXPIRES,
    issuer: JWT_ISSUER
  };
  try {
    return jwt.verify(token, JWT_SECRET, verifyOptions);
  } catch (error) {
    return null;
  }
}

const decode = token => {
  return jwt.decode(token, { complete: true });
}

module.exports = {
  sign,
  parseTokenFromAuthorizationHeader,
  verify,
  decode
}
