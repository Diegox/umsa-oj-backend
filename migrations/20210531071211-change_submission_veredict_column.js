'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        return queryInterface.changeColumn('submissions', 'veredict', {
            type: Sequelize.STRING
        });
    },

    down: async (queryInterface, Sequelize) => {
        queryInterface.changeColumn('submissions ', 'veredict', {
            type: Sequelize.ENUM
        });
    }
};
