'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('testCases', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            problemId: {
                type: Sequelize.INTEGER
            },
            tag: {
                type: Sequelize.STRING
            },
            inputFileName: {
                type: Sequelize.STRING,
                trim: true
            },
            outputFileName: {
                type: Sequelize.STRING,
                trim: true
            },
            inputFileName: {
                type: Sequelize.STRING,
                trim: true
            },
            judge: {
                type: Sequelize.BOOLEAN,
            },
            level: {
                type: Sequelize.INTEGER,
                defaultValue: 1
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('testCases');
    }
};
