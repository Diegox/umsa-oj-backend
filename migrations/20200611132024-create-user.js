'use strict';

const { DataTypes } = require('sequelize');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      firstName: {
        type: DataTypes.STRING,
        allowNull: false,
        trim: true
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: false,
        trim: true
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        trim: true,
        unique: true
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        trim: true,
        unique: true
      },
      institution: {
        type: DataTypes.STRING,
        trim: true,
      },
      role: {
        type: DataTypes.ENUM('superAdmin', 'admin', 'problemSetter', 'problemEditor', 'basic'),
        defaultValue: 'basic'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('users');
  }
};
