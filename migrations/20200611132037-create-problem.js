'use strict';
const { DataTypes } = require('sequelize');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('problems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false,
        trim: true
      },
      author: {
        type: DataTypes.STRING,
        trim: true
      },
      statement: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      inputDescription: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      inputExample: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      outputDescription: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      outputExample: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      timeLimit: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      memoryLimit: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      hints: {
        type: DataTypes.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('problems');
  }
};
