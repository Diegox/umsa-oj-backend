'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('submissions', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        problemId: {
            type: Sequelize.INTEGER
        },
        userId: {
            type: Sequelize.INTEGER
        },
        veredict: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.STRING
        },
        language: {
            type: Sequelize.ENUM('java', 'cpp', 'c', 'python'),
            defaultValue: 'cpp'
        },
        timeSubmitted: {
            type: Sequelize.DATE
        },
        runtime: {
            type: Sequelize.DOUBLE,
        },
        sourceCode: {
            type: Sequelize.STRING
        },
        createdAt: {
            allowNull: false,
            type: Sequelize.DATE
        },
        updatedAt: {
            allowNull: false,
            type: Sequelize.DATE
        }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('submissions');
  }
};
