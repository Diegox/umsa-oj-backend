'use strict';

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    firstName: {
        type: DataTypes.STRING,
        allowNull: false,
        trim: true
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false,
        trim: true
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        trim: true,
        unique: true,
        validate: {
          isEmail: true
        }
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            min: 6
        }
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false,
        trim: true,
        unique: true
    },
    institution: {
        type: DataTypes.STRING,
        trim: true,
    },
    role: {
        type: DataTypes.ENUM('superAdmin', 'admin', 'problemSetter', 'problemEditor', 'basic'),
        defaultValue: 'basic'
    }
  }, {});
  User.associate = function(models) {
    User.hasMany(models.problem);
    User.hasMany(models.submission);

  };
  return User;
};
