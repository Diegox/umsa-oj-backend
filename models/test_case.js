'use strict';

module.exports = (sequelize, DataTypes) => {
    const TestCase = sequelize.define('testCase', {
        tag: {
            type: DataTypes.STRING,
            allowNull: false,
            trim: true
        },
        inputFileName: {
            type: DataTypes.STRING,
            trim: true
        },
        outputFileName: {
            type: DataTypes.STRING,
            trim: true
        },
        judge: {
            type: DataTypes.BOOLEAN,
        },
        level: {
            type: DataTypes.INTEGER
        }
    }, {});
    TestCase.associate = function (models) {
        TestCase.belongsTo(models.problem, {
            foreignKey: 'problemId'
        });
    };
    return TestCase;
};
