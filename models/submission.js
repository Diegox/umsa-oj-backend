'use strict';

module.exports = (sequelize, DataTypes) => {
    const Submission = sequelize.define('submission', {
        veredict: {
            type: DataTypes.STRING
        },
        status: {
            type: DataTypes.STRING
        },
        language: {
            type: DataTypes.ENUM('java', 'cpp', 'c', 'python'),
            defaultValue: 'cpp'
        },
        timeSubmitted: {
            type: DataTypes.DATE
        },
        runtime: {
            type: DataTypes.DOUBLE,
        },
        sourceCode: {
            type: DataTypes.STRING,
        }
    }, {});
    Submission.associate = function (models) {
        Submission.belongsTo(models.problem, {
            foreignKey: 'problemId'
        });
        Submission.belongsTo(models.user, {
            foreignKey: 'userId'
        });
    };
    return Submission;
};
