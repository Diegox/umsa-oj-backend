'use strict';

module.exports = (sequelize, DataTypes) => {
  const Problem = sequelize.define('problem', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      trim: true
    },
    author: {
      type: DataTypes.STRING,
      trim: true
    },
    statement: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    inputDescription: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    inputExample: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    outputDescription: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    outputExample: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    timeLimit: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    memoryLimit: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    hints: {
      type: DataTypes.TEXT,
    }
  }, {});
  Problem.associate = function (models) {
    Problem.belongsTo(models.user, {
      foreignKey: 'userId'
    });
    Problem.hasMany(models.testCase);
    Problem.hasMany(models.submission);
  };

  return Problem;
};
