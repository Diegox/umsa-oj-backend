const { submission: Submission, user: User } = require('../models');
const { Op } = require('sequelize');
const { successResponse, errorResponse } = require('../utils/responseApi');
const { paginate } = require('../utils/paginate');

const create = async (req, res) => {
    try {
        const submission = req.body;
        const newSubmission = await Submission.create(submission);
        res.status(200).json(successResponse('ok', newSubmission, res.statusCode));
    } catch (error) {
        // TODO: handle validation errorsc
        console.log('error: ', error);
        res.status(500).json(errorResponse('Something went wrong', 500));
    }
}

const update = async (req, res) => {
    const id = req.params.id;
    const submission = req.body;
    try {
        const submissionUpdated = await Submission.update({
            submission,
            where: { id }
        });
        res.status(200).json({ message: 'ok', submission: submissionUpdated });
    } catch (error) {
        console.log('error: ', error);
        res.status(500).json({ message: 'something went wrong' });
    }
}

const patch = async (req, res) => {
    const id = req.params.id;
    const patch = req.body;
    console.log('patch: ', patch);
    try {
        const submissionPatched = await Submission.update(patch, {
            where: { id }
        });
        res.status(200).json({ message: 'ok', submission: submissionPatched });
    } catch (error) {
        console.log('error: ', error);
        res.status(500).json({ message: 'something went wrong' });
    }
}

const getOne = async (req, res) => {
    const id = req.params.id;
    try {
        const submission = await Submission.findOne({
            where: { id }
        });
        res.status(200).json(successResponse('ok', submission, res.statusCode));
    } catch (error) {
        res.status(500).json(errorResponse('Something went wrong', status.statusCode));
    }
}

const getAll = async (req, res) => {
    const page = req.query.page ?? 0;
    const pageSize = req.query.rowsPerPage ?? 10;
    const search = req.query.searchText ?? '';
    const sortField = req.query.sortName ?? 'timeSubmitted';
    const sortDirection = req.query.sortDirection ?? 'desc';

    let order = [[sortField, sortDirection]];
    let searchOptions = {};

    if (search) {
        searchOptions = {
            [Op.or]: [
                // { id: { [Op.like]: +search }},
                { language: { [Op.iLike]: '%' + search + '%' } },
                { veredict: { [Op.iLike]: '%' + search + '%' } }
            ]
        };
    }

    try {
        let { count, rows } = await Submission.findAndCountAll({
            where: searchOptions,
            order,
            include: [{
                model: User,
                attributes: ['username']
            }],
            // attributes,
            ...paginate({ page, pageSize })
        });
        let response = JSON.stringify(rows);
        response = JSON.parse(response);
        response = response.map(e => ({...e, username: e.user.username}));
        res.status(200).json(successResponse('ok', {
            data: response,
            count,
        }, res.statusCode));

    } catch (error) {
        console.log('error:', error);
        res.status(500).json({ message: 'something went wrong' });
    }
}

const remove = async (req, res) => {
    const id = req.params.id;
    try {
        const submission = Submission.destroy({
            where: { id }
        });
        res.status(200).json({ message: 'ok', submission });
    } catch (error) {
        res.status(400).json({ message: 'something went wrong' });
    }
}

module.exports = {
    create,
    update,
    patch,
    remove,
    getOne,
    getAll
}
