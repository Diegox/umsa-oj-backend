const { testCase: TestCase } = require('../models');
const { Op } = require('sequelize');
const { successResponse, errorResponse } = require('../utils/responseApi');

const create = async (req, res) => {
    try {
        const testCase = req.body;
        const newTestCase = await TestCase.create(testCase);
        res.status(200).json(successResponse('ok', newTestCase, res.statusCode));
    } catch (error) {
        // TODO: handle validation errors
        res.status(500).json(errorResponse('Something went wrong', 500));
    }
}

const update = async (req, res) => {
    const id = req.params.id;
    const testCase = req.body;
    try {
        const testCaseUpdated = await TestCase.update({
            testCase,
            where: { id }
        });
        res.status(200).json({ message: 'ok', problem: problemUpdated });
    } catch (error) {
        res.status(400).json({ message: 'something went wrong' });
    }
}

const getOne = async (req, res) => {
    const id = req.params.id;
    try {
        const testCase = await TestCase.findOne({
            where: { id }
        });
        res.status(200).json(successResponse('ok', testCase, res.statusCode));
    } catch (error) {
        res.status(500).json(errorResponse('Something went wrong', status.statusCode));
    }
}

const getAll = async (req, res) => {
    const sortField = req.query.sortName;
    const sortDirection = req.query.sortDirection;

    const problemId = req.body.problemId;

    let order = [];
    let searchOptions = {};

    if (sortField) {
        order = [
            [sortField, sortDirection]
        ];
    } else {
        order = [
            ['id', 'asc']
        ];
    }

    //   attributes = ['id', 'title', 'author'];

    try {
        const { count, rows } = await Problem.findAndCountAll({
            where: { problemId },
            order,
            //   attributes,
            //   ...paginate({ page, pageSize })
        });
        res.status(200).json(successResponse('ok', {
            data: rows,
            count,
            page
        }, res.statusCode));

    } catch (error) {
        console.log('error:', error);
        res.status(400).json({ message: 'something went wrong' });
    }
}

const remove = async (req, res) => {
    const id = req.params.id;
    try {
        const testCase = TestCase.destroy({
            where: { id }
        });
        res.status(200).json({ message: 'ok', testCase });
    } catch (error) {
        res.status(400).json({ message: 'something went wrong' });
    }
}

module.exports = {
    create,
    update,
    remove,
    getOne,
    getAll
}
