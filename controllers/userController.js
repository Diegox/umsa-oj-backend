const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { user: User } = require('../models');
const { JWT_SECRET, JWT_EXPIRES } = require('../settings');
const { successResponse, errorResponse, validationResponse } = require('../utils/responseApi');
const { validationResult } = require('express-validator');

const hashPassword = async password => await bcrypt.hash(password, 10);

const compareHashedPassword = async (plain, hashed) => await bcrypt.compare(plain, hashed);

const signup = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const formatedErrors = errors.errors.map(error => {
            const message = error.msg;
            const field = error.param;
            const value = error.value;
            return { field, message }
        });
        res.status(422).json(validationResponse(formatedErrors, res.statusCode));
    }

    try {
        const { username, password, firstName, lastName, email, institution } = req.body;
        const hashedPassword = await hashPassword(password);
        const data = {
            email,
            password: hashedPassword,
            role: 'basic',
            username,
            firstName,
            lastName,
            institution
        };
        const newUser = await User.create(data);
        await newUser.save();
        newUser.password = undefined;

        const accessToken = jwt.sign({ userId: newUser.id, role: newUser.role }, JWT_SECRET, {
          expiresIn: JWT_EXPIRES,
          issuer: 'umsa-oj'
        });
        res.status(200).json(successResponse('OK', { user: newUser, token: accessToken }, 200));
    } catch (error) {
        try {
            const formatedErrors = error.errors.map(error => {
                const message = error.message;
                const field = error.path;
                const value = error.value;
                return { field, message }
            });
            res.status(422).json(validationResponse(formatedErrors, res.statusCode));
        } catch (anotherError) {
            res.status(500).json(errorResponse('An error happened', 500, error));
        }
    }
}

const login = async (req, res) => {
    try {
        const { username, password } = req.body;
        const user = await User.findOne({
            where: {
              username
            }
        });
        if (!user) {
            return res.status(422).json({ message: 'Email address does not exist' });
        }
        const isValidPassword = await compareHashedPassword(password, user.password);
        if (!isValidPassword) {
            return res.status(422).json({ message: 'Invalid password' });
        }
        const accessToken = jwt.sign({ userId: user.id, role: user.role }, JWT_SECRET, {
            expiresIn: JWT_EXPIRES,
            issuer: 'umsa-oj'
        });
        user.password = undefined;
        res.status(200).json({
            data: {
                user,
                token: accessToken
            }
        });
    } catch (error) {
        console.log('Error: ', error);
        res.status(500).json({
            error: 'Something went wrong'
        });
    }
}

const getUsers = async (req, res, next) => {
  console.log('req:', req.role);
    try {
        const users = await User.findAll({
          where: {},
          attributes: {
            exclude: ['password']
          }
        });
        res.status(200).send({
            data: users
        })
    } catch(error) {
        res.status(500).json({
          error: 'Something went wrong'
        });
    }
}

const getUser = async (req, res, next) => {
    try {
        const userId = req.params.id;
        const user = await User.find({
          where: {
            id: userId
          },
          attributes: {
            exclude: ['password']
          }
        });
        if (!user) {
          res.status(500).json({
            error: 'Something went wrong'
          });
        }
        res.status(200).json({
            data: user
        });
    } catch (error) {
        res.status(500).json({
            error: 'Something went worng'
        });
    }
}


module.exports = {
    signup,
    login,
    getUsers,
    getUser
}
