const { problem: Problem } = require('../models');
const { paginate } = require('../utils/paginate');
const { Op } = require('sequelize');
const { successResponse, errorResponse } = require('../utils/responseApi');

const create = async (req, res) => {
  try {
    const problem = req.body;
    const newProblem = await Problem.create(problem);
    res.status(200).json(successResponse('ok', newProblem, res.statusCode));
  } catch (error) {
    // TODO: handle validation errors
    res.status(500).json(errorResponse('Something went wrong', 500));
  }
}

const update = async (req, res) => {
  const id = req.params.id;
  const problem = req.body;
  try {
    const problemUpdated = await Problem.update({
      problem,
      where: { id }
    });
    res.status(200).json({ message: 'ok', problem: problemUpdated});
  } catch (error) {
    res.status(400).json({ message: 'something went wrong' });
  }
}

const getOne = async (req, res) => {
  const id = req.params.id;
  try {
    const problem = await Problem.findOne({
      where: { id }
    });
    res.status(200).json(successResponse('ok', problem, res.statusCode));
  } catch (error) {
      res.status(500).json(errorResponse('Something went wrong', status.statusCode));
  }
}

const getAll = async (req, res) => {
  const page = req.query.page || 0;
  const pageSize = req.query.rowsPerPage || 10;
  const search = req.query.searchText;
  const sortField = req.query.sortName;
  const sortDirection = req.query.sortDirection;

  let order = [];
  let searchOptions = {};

  if (sortField) {
    order = [
      [sortField, sortDirection]
    ];
  } else {
    order = [
      ['id', 'asc']
    ];
  }
  console.log('search:', search);
  if (search) {
    searchOptions = {
      [Op.or]: [
        // { id: { [Op.like]: +search }},
        { title: { [Op.iLike]: '%' + search +'%' }},
        { author: { [Op.iLike]: '%' + search +'%' }},
      ]
    };
  }

  attributes = ['id', 'title', 'author'];

  console.log('page, pageSize:', page, pageSize);
  try {
    const { count, rows } = await Problem.findAndCountAll({
      where: searchOptions,
      order,
      attributes,
      ...paginate({ page, pageSize })
    });
    // // console.log('rows:', rows);
    // const problems = await Problem.findAll();
    res.status(200).json(successResponse('ok', {
        data: rows,
        count,
        page
    }, res.statusCode));

  } catch (error) {
    console.log('error:', error);
    res.status(400).json({ message: 'something went wrong' });
  }
}

const remove = async (req, res) => {
  const id = req.params.id;
  try {
    const problem = await Problem.destroy({
      where: { id }
    });
    res.status(200).json({ message: 'ok', problem });
  } catch (error) {
    res.status(400).json({ message: 'something went wrong' });
  }
}

const getTestCasesByProblemId = async (req, res) => {
    const id = req.params.id;
    try {
        const problem = await Problem.findOne({
            where: {
                id
            },
            attributes: ['title'],
            include: ['testCases']
        });
        res.status(200).json(successResponse('ok', problem, res.statusCode));
    } catch (error) {
        console.log('error: ', error);
        res.status(500).json(errorResponse('Something went wrong', res.statusCode));
    }
}

module.exports = {
  create,
  update,
  remove,
  getOne,
  getAll,
  getTestCasesByProblemId
}
