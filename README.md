# umsa-oj-backend
The umsa-oj-backend :v

## Create .env file
Create .env file with this values:

- POSTGRES_PASSWORD=holabola
- POSTGRES_USER=pepe
- POSTGRES_DB=umsa-oj

## Run postgres

```shell
docker-compose up postgres
```

## Check database connectivity

```shell
docker exec -it postgres psql -U pepe -d umsa-oj
```

## Run migrations
```shell
npx sequelize-cli db:migrate
```

## Seed with fake data

``` shell
npx sequelize-cli db:seed:all
```

## Undo all seeds
This will remove all the data
``` shell
npx sequelize-cli db:seed:undo:all
```

## Undo all migrations
This will undo the migrations (remove tables from databse)
``` shell
npx sequelize-cli db:migrate:undo:all
```


