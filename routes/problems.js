const express = require('express');
const router = express.Router();
const problemController = require('../controllers/problemController');
const verifyToken = require('../middlewares/authMiddleware');
const verifyRole = require('../middlewares/roleMiddleware');

router.get('/problems/:id/test-cases', problemController.getTestCasesByProblemId);

router.post('/problems', verifyToken, verifyRole('createAny', 'problem'), problemController.create);
router.get('/problems/:id', problemController.getOne);
router.get('/problems', problemController.getAll);
router.put('/problems/:id', verifyToken, verifyRole('updateOwn', 'problem'), problemController.update);
router.delete('/problems/:id', verifyToken, verifyRole('deleteOwn', 'problem'), problemController.remove);


module.exports = router;
11
