const express = require('express');
const { check } = require('express-validator');
const router = express.Router();
const userController = require('../controllers/userController');
const { verifyToken, verifyRole} = require('../middlewares');

router.post('/signup', [
  check('firstName')
    .not()
    .isEmpty()
    .withMessage('must not be empty'),
  check('lastName')
    .not()
    .isEmpty()
    .withMessage('must not be empty'),
  check('password')
    .isLength({ min: 6 })
    .withMessage('must be at least 6 characters long')
    .matches(/\d/)
    .withMessage('must contain a number'),
  check('email')
    .isEmail()
    .withMessage('invalid email'),
  check('username')
    .not()
    .isEmpty()
    .withMessage('must not be empty')
], userController.signup);

router.post('/login', userController.login);

router.get('/users', verifyToken, verifyRole('readAny', 'user'), userController.getUsers);

module.exports = router;
