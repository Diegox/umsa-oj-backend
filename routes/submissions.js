const express = require('express');
const router = express.Router();
const submissionController = require('../controllers/submissionController');
const verifyToken = require('../middlewares/authMiddleware');
const verifyRole = require('../middlewares/roleMiddleware');


router.post('/submissions', submissionController.create);
router.get('/submissions/:id', submissionController.getOne);
router.get('/submissions', submissionController.getAll);
router.put('/submissions/:id', submissionController.update);
router.patch('/submissions/:id', submissionController.patch);
router.delete('/submissions/:id', submissionController.remove);

module.exports = router;

