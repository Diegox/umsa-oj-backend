const express = require('express');
const router = express.Router();
const testCaseController = require('../controllers/testCasesController');
const verifyToken = require('../middlewares/authMiddleware');
const verifyRole = require('../middlewares/roleMiddleware');


router.post('/test-cases', verifyToken, verifyRole('createAny', 'problem'), testCaseController.create);
router.get('/test-cases/:id', testCaseController.getAll);
router.get('/test-cases', testCaseController.getAll);
router.put('/test-cases/:id', verifyToken, verifyRole('updateOwn', 'problem'), testCaseController.update);
router.delete('/test-cases/:id', verifyToken, verifyRole('deleteOwn', 'problem'), testCaseController.remove);

module.exports = router;

