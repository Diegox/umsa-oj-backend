'use strict';
const faker = require('faker');
const bcrypt = require('bcrypt');

const hashPassword = password => bcrypt.hashSync(password, 10);

module.exports = {
  up: (queryInterface, Sequelize) => {
    const users = [];
    const roles = ['superAdmin', 'admin', 'problemSetter', 'problemEditor', 'basic'];
    for (let i = 0; i < 100; i++) {
      const firstName = faker.name.firstName();
      const user = {
        firstName: firstName,
        lastName: faker.name.lastName(),
        password: hashPassword('aea'),
        email: faker.internet.email(firstName),
        username: faker.internet.userName(firstName),
        institution: faker.company.companyName(),
        // role: roles[faker.random.number({ min: 0, max: 4 })],
        createdAt: new Date(),
        updatedAt: new Date()
      }
      users.push(user);
    }
    return queryInterface.bulkInsert('users', users, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
   return queryInterface.bulkDelete('users', null, {});
  }
};
