'use strict';

const faker = require('faker');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const testCases = [];
        for (let i = 1; i <= 100; i++) {
            const problemId = faker.random.number({min: 1, max: 10});
            const testCase = {
                problemId: problemId,
                tag: faker.random.alphaNumeric(5),
                level: faker.random.number({min: 1, max: 5}),
                judge: faker.random.boolean(),
                inputFileName: '/problem' + problemId + `/input/input${i}.txt`,
                outputFileName: '/problem' + problemId + `/output/output${i}.txt`,
                createdAt: new Date(),
                updatedAt: new Date()
            }
            testCases.push(testCase);
        }
        return queryInterface.bulkInsert('testCases', testCases, {});
    },

    down: async (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('testCases', null, {});
    }
};
