'use strict';

const faker = require('faker');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const problems = [];
    for (let i = 0; i < 10; i++) {
      const problem = {
        userId: 2,
        title: 'problem' + (i + 1),
        author: faker.name.firstName(),
        statement: faker.lorem.paragraphs(1, 'aea'),
        inputDescription: faker.lorem.paragraph(1),
        inputExample: faker.lorem.paragraph(1),
        outputDescription: faker.lorem.paragraph(1),
        outputExample: faker.lorem.paragraph(1),
        timeLimit: faker.random.number({min: 1, max: 5}),
        memoryLimit: faker.random.number({min: 10, max: 50}),
        hints: faker.lorem.paragraph(1),
        createdAt: new Date(),
        updatedAt: new Date()
      }
      problems.push(problem);
    }
    return queryInterface.bulkInsert('problems', problems, {});
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('problems', null, {});
  }
};
