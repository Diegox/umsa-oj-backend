'use strict';
const bcrypt = require('bcrypt');
const faker = require('faker');
const hashPassword = password => bcrypt.hashSync(password, 10);

module.exports = {
  up: (queryInterface, Sequelize) => {
    const users = [];
    const admin = {
      firstName: 'admin',
      lastName: 'admin',
      password: hashPassword('admin'),
      email: faker.internet.email('admin'),
      username: 'admin',
      role: 'admin',
      createdAt: new Date(),
      updatedAt: new Date()
    }
    users.push(admin);
    const problemSetter = {
      firstName: 'problemSetter',
      lastName: 'problemSetter',
      password: hashPassword('problemSetter'),
      email: faker.internet.email('problemSetter'),
      username: 'problemSetter',
      role: 'problemSetter',
      createdAt: new Date(),
      updatedAt: new Date()
    }
    users.push(problemSetter);
    const problemEditor = {
      firstName: 'problemEditor',
      lastName: 'problemEditor',
      password: hashPassword('problemEditor'),
      email: faker.internet.email('problemEditor'),
      username: 'problemEditor',
      role: 'problemEditor',
      createdAt: new Date(),
      updatedAt: new Date()
    }
    users.push(problemEditor);
    const basic = {
      firstName: 'basic',
      lastName: 'basic',
      password: hashPassword('basic'),
      email: faker.internet.email('basic'),
      username: 'basic',
      role: 'basic',
      createdAt: new Date(),
      updatedAt: new Date()
    }
    users.push(basic);
    return queryInterface.bulkInsert('users', users, {});
  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.bulkDelete('users', null, {});
  }
};
