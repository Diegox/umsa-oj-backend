const JWT_SECRET = process.env.JWT_SECRET || 'my_super_secret_key_XD';
const JWT_ISSUER = process.env.JWT_ISSUER || 'Diegox';
const JWT_EXPIRES = process.env.JWT_EXPIRES || '1d' ;

module.exports = {
    JWT_SECRET,
    JWT_ISSUER,
    JWT_EXPIRES
}
