const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('../settings');

const verifyJwt = (req, res, next) => {
  let token = req.headers['authorization'];
  if (!token) {
    res.status(400).json({ message: 'Token not provided'});
  } else {
    token = token.substr('Bearer '.length);
    try {
      const decoded = jwt.verify(token, JWT_SECRET);
      req.user = {};
      req.user.role = decoded.role;
      next();
    } catch(err) {
      res.status(400).json({ message: 'Invalid token'});
    }
  }
}

module.exports = verifyJwt;

