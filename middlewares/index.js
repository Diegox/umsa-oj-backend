const verifyToken = require('./authMiddleware');
const verifyRole = require('./roleMiddleware');

module.exports = {
  verifyRole,
  verifyToken
}
