const { roles } = require('../roles');

const verifyRole = (action, resource) => {
  return (req, res, next) => {
    const role = req.user.role;
    if (!role) {
      res.status(400).json({ message: 'No role provided' });
    } else {
      const permission = roles.can(role)[action](resource);
      if (!permission.granted) {
        res.status(403).json({ message: 'Unauthorized'});
      } else {
        next();
      }
    }
  }
}

module.exports = verifyRole;
